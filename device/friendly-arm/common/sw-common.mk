
BUILD_NUMBER := $(shell date +%Y%m%d)

# 3G Data Card Configuration Files
# Added chat-connect to get Huawei generic RIL to dial out PPP
#PRODUCT_COPY_FILES += \
#	device/friendly-arm/common/rild/ip-down:system/etc/PPP/ip-down \
#	device/friendly-arm/common/rild/ip-up:system/etc/PPP/ip-up \
#	device/friendly-arm/common/rild/chat-connect:system/etc/PPP/chat-connect \
#	device/friendly-arm/common/rild/call-pppd:system/etc/PPP/call-pppd \
#	device/friendly-arm/common/rild/apns-conf_sdk.xml:system/etc/apns-conf.xml \
#	device/friendly-arm/common/rild/3gdata_call.conf:system/etc/PPP/3gdata_call.conf \
#	device/friendly-arm/common/rild/init.gprs-pppd:system/etc/PPP/init.gprs-pppd
